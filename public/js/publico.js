const socket = io()

const ticket1 = document.getElementById('ticket1')
const ticket2 = document.getElementById('ticket2')
const ticket3 = document.getElementById('ticket3')
const ticket4 = document.getElementById('ticket4')
const desktop1 = document.getElementById('desktop1')
const desktop2 = document.getElementById('desktop2')
const desktop3 = document.getElementById('desktop3')
const desktop4 = document.getElementById('desktop4')

socket.on('show-last-tickets', tickets => {
  if (tickets?.length === 0) return
  const [obj1, obj2, obj3, obj4] = tickets
  if (obj1) {
    ticket1.innerText = `Ticket: ${obj1.number}`
    desktop1.innerText = `Desktop: ${obj1.desktop}`
  }
  if (obj2) {
    ticket2.innerText = `Ticket: ${obj2.number}`
    desktop2.innerText = `Desktop: ${obj2.desktop}`
  }
  if (obj3) {
    ticket3.innerText = `Ticket: ${obj3.number}`
    desktop3.innerText = `Desktop: ${obj3.desktop}`
  }
  if (obj4) {
    desktop4.innerText = `Desktop: ${obj4.desktop}`
    ticket4.innerText = `Ticket: ${obj4.number}`
  }
})
