const socket = io()

const input = document.getElementById('txtMessage')
const button = document.getElementById('submitBtn')

socket.on('connect', () => {
  console.log('conectado')
})

socket.on('disconnect', () => {
  console.log('desconectado :(')
})

socket.on('send-data', data => {
  console.log(data)
})

button.addEventListener('click', () => {
  const message = input.value
  socket.emit('send-message', message, id => {
    console.log('Desde el server', id)
  })
})
