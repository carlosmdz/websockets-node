const socket = io()

const searchParams = new URLSearchParams(window.location.search)

const desktopId = document.getElementById('desktopId')
const attendBtn = document.getElementById('attendBtn')
const currentTicket = document.getElementById('currentTicket')
const alertMsg = document.getElementById('alertMsg')
const lblPendientes = document.getElementById('lblPendientes')

if (!searchParams.has('desktop')) {
  window.location = 'index.html'
  throw new Error('Desktop property is mandatory.')
}

const desktop = searchParams.get('desktop')
desktopId.innerText = `Desktop: ${desktop}`
alertMsg.style.display = 'none'

socket.on('connect', () => {
  console.log('conectado')
  attendBtn.disabled = false
})

socket.on('disconnect', () => {
  console.log('desconectado')
  attendBtn.disabled = true
})

socket.on('pending-tickets', ticketsNum => {
  lblPendientes.innerText = ticketsNum
})

attendBtn.addEventListener('click', () => {
  socket.emit('attend-ticket', desktop, payload => {
    console.log(payload)
    if (!payload.ok) {
      currentTicket.innerText = 'nobody'
      return (alertMsg.style.display = '')
    }
    currentTicket.innerText = `Ticket: ${payload.ticket.number}`
  })
})
