const socket = io()

const message = document.getElementById('lblNuevoTicket')
const button = document.getElementById('newTicketBtn')

socket.on('connect', () => {
  console.log('conectado')
  button.disabled = false
})

socket.on('disconnect', () => {
  console.log('desconectado')
  button.disabled = true
})

socket.on('last-ticket', ticket => {
  message.innerText = `Ticket ${ticket}`
})

button.addEventListener('click', () => {
  socket.emit('new-ticket', null, ticket => {
    message.innerText = `Ticket ${ticket}`
  })
})
