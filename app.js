require('dotenv').config()
const express = require('express')
const cors = require('cors')
const app = express()
const server = require('http').createServer(app)
const io = require('socket.io')(server)

const port = process.env.PORT

const { socketController } = require('./sockets/controller')

// Middlewares
app.use(cors())
app.use(express.static('public'))

// Routes
// app.use('/api/auth', authRoutes)

// Sockets
io.on('connection', socketController)

// Listen
server.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
