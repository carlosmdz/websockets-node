const path = require('path')
const fs = require('fs')

class Ticket {
  constructor(number, desktop) {
    this.number = number
    this.desktop = desktop
  }
}

class TicketHandler {
  constructor() {
    this.day = new Date().getDate()
    this.lastTicket = 0
    this.allTickets = []
    this.lastTickets = []

    this.init()
  }

  get toJson() {
    return {
      day: this.day,
      lastTicket: this.lastTicket,
      allTickets: this.allTickets,
      lastTickets: this.lastTickets,
    }
  }

  init() {
    const {
      day,
      lastTicket,
      allTickets,
      lastTickets,
    } = require('../db/data.json')
    if (day === this.day) {
      this.day = day
      this.lastTicket = lastTicket
      this.allTickets = allTickets
      this.lastTickets = lastTickets
    } else {
      this.saveJson()
    }
  }

  saveJson() {
    const jsonPath = path.join(__dirname, '../db/data.json')
    fs.writeFileSync(jsonPath, JSON.stringify(this.toJson))
  }

  next() {
    this.lastTicket += 1
    const ticket = new Ticket(this.lastTicket, null)
    this.allTickets.push(ticket)
    this.saveJson()
    return ticket.number
  }

  attendTicket(desktop) {
    if (this.allTickets.length === 0) return null
    const ticket = this.allTickets.shift() // Remove and get first ticket
    ticket.desktop = desktop
    this.lastTickets.unshift(ticket)
    if (this.lastTickets.length > 4) {
      this.lastTickets.pop()
    }
    this.saveJson()
    return ticket
  }
}

module.exports = TicketHandler
