const TicketHandler = require('../models/ticket')

const ticketHandler = new TicketHandler()

const socketController = client => {
  console.log('Cliente conectado')

  client.emit('last-ticket', ticketHandler.lastTicket)
  client.emit('show-last-tickets', ticketHandler.lastTickets)
  client.emit('pending-tickets', ticketHandler.allTickets.length)

  client.on('disconnect', () => {
    console.log('Cliente desconectado')
  })

  client.on('new-ticket', (_, callback) => {
    const ticket = ticketHandler.next()
    callback(ticket)
    client.broadcast.emit('pending-tickets', ticketHandler.allTickets.length)
  })

  client.on('attend-ticket', (desktop, callback) => {
    if (!desktop) {
      return callback({ ok: false, msg: 'Desktop is mandatory' })
    }
    const ticket = ticketHandler.attendTicket(desktop)
    if (!ticket) {
      return callback({ ok: false, msg: 'No tickets available' })
    }
    client.broadcast.emit('show-last-tickets', ticketHandler.lastTickets)
    client.emit('pending-tickets', ticketHandler.allTickets.length)
    client.broadcast.emit('pending-tickets', ticketHandler.allTickets.length)
    callback({ ok: true, ticket })
  })
}

module.exports = { socketController }
